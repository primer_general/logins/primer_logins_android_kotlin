package com.okey.primerlogins.kotlin

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okey.primerlogins.kotlin.fragment.Channel_item
import com.okey.primerlogins.kotlin.fragment.ChannelsList_list
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.channel_list_item.view.*

class ChannelAdapter() : RecyclerView.Adapter<ChannelAdapter.ViewHolder>() {

    private var items = listOf<Channel_item?>()

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.channel_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bindChannel(item)
    }


    fun addItems(channels: List<Channel_item?> ) {
        items = channels
    }

    inner class ViewHolder(v: View) :
        RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var channel: Channel_item? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
        }

        fun bindChannel(channel: Channel_item?) {
            this.channel = channel
            view.tv_channel_name.text = channel?.name()
            Picasso.get().load(channel?.poster()).into(view.img_channel_poster)
        }
    }

}


