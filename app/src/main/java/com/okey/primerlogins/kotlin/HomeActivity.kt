package com.okey.primerlogins.kotlin

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.NavArgument
import com.okey.primerlogins.kotlin.models.User
import com.squareup.picasso.Picasso

class HomeActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val user = intent.getParcelableExtra<User>("userTag")

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        val headerView = navView.getHeaderView(0)
        val navUsername = headerView.findViewById<TextView>(R.id.txtUserName)
        navUsername.setText(user.name)

        val navUserEmail = headerView.findViewById<TextView>(R.id.txtUserEmail)
        navUserEmail.setText(user.email)

        val navUserPicture = headerView.findViewById<ImageView>(R.id.imgUserPicture)
        Picasso.get().load(user.pictureUrl).into(navUserPicture)

        val navController = findNavController(R.id.nav_host_fragment)

        val bundle: Bundle = bundleOf("token" to user.token)
        navController.setGraph(R.navigation.mobile_navigation, bundle)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when(destination.id) {
                R.id.nav_home -> {
                    val argument = NavArgument.Builder().setDefaultValue(user.token).build()
                    destination.addArgument("token", argument)
                }
            }
        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, //R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools //R.id.nav_share, R.id.nav_send
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
