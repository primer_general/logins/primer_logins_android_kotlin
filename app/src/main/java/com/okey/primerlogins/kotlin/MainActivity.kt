package com.okey.primerlogins.kotlin

import android.content.Intent
import android.os.Bundle
import android.system.Os.bind
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.okey.primerlogins.kotlin.models.User
import com.okey.primerlogins.kotlin.type.UserInput

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import okhttp3.OkHttpClient
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var fbCallbackManager: CallbackManager? = null
    internal var fbToken: AccessToken? = null

    internal val TAG = "RNLOGINS"
    private val BASE_GRAPHQL_SERVER_URL = "http://10.0.2.2:4000"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fbCallbackManager = CallbackManager.Factory.create()

        fbLoginButton.registerCallback(fbCallbackManager,
                    object: FacebookCallback<LoginResult> {
                        override fun onSuccess(loginResult: LoginResult) {
                                fbToken = loginResult.accessToken

                                val request = GraphRequest.newMeRequest(loginResult.accessToken) { me, response ->
                                    val email = me.optString("email")
                                    val id = me.optString("id")
                                    val name = me.optString("name")
                                    val picture = me.optJSONObject("picture")
                                    val pictureData = picture.optJSONObject("data")
                                    val pictureUrl = pictureData.optString("url")

                                    // GraphQL type
                                    val user = UserInput.builder()
                                        .name(name)
                                        .email(email)
                                        .pictureUrl(pictureUrl)
                                        .token(fbToken!!.getToken())
                                        .providerName("facebook")
                                        .build()

                                    registerUser(user)
                                }

                                val parameters = Bundle()
                                parameters.putString("fields", "name,email,picture")
                                request.parameters = parameters
                                request.executeAsync()
                    }

                    override fun onCancel() {
                        // App code
                    }

                    override fun onError(exception: FacebookException) {
                        // App code
                    }
        })
    }

    private fun registerUser(user: UserInput): Boolean {

        val okHttpClient = OkHttpClient.Builder().build()
        val apolloClient = ApolloClient.builder()
            .serverUrl(BASE_GRAPHQL_SERVER_URL)
            .okHttpClient(okHttpClient)
            .build()

        val registerMutation = RegisterMutation.builder()
            .user(user)
            .build()

        val self = this

        apolloClient.mutate(registerMutation)
            .enqueue(object : ApolloCall.Callback<RegisterMutation.Data>() {
                override fun onResponse(response: Response<RegisterMutation.Data>) {
                    if (!response.hasErrors()) {

                        val data = response.data()?.let {
                            it
                        }
                        Log.d(TAG, data.toString())
                        val intent = Intent(self, HomeActivity::class.java)
                        val parcellableUser = User(user.name(),
                                                    user.email(),
                                                    user.pictureUrl()!!,
                                                    user.token())
                        intent.putExtra("userTag", parcellableUser);
                        startActivity(intent)
                    }
                }

                override fun onFailure(e: ApolloException) {

                    this@MainActivity.runOnUiThread {
                        val builder = AlertDialog.Builder(this@MainActivity)
                        builder.setTitle(e.message)
                        builder.setMessage(e.cause.toString())
                        builder.setPositiveButton("OK") { dialog, which ->
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }

                }
            })

        return true;
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        fbCallbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}
