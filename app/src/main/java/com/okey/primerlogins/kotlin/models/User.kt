package com.okey.primerlogins.kotlin.models

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class User(
    val name: String,
    val email: String,
    val pictureUrl: String,
    val token: String
) : Parcelable

