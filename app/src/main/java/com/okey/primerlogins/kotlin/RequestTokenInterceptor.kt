package com.okey.primerlogins.kotlin

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


internal class RequestTokenInterceptor(val token: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val originalRequest = chain.request()
        val newRequest: Request

        newRequest = originalRequest.newBuilder()
            .header("Authorization", "Bearer " + token)
            .method(originalRequest.method(), originalRequest.body())
            .build();

        return chain.proceed(newRequest);
    }
}