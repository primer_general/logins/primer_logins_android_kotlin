package com.okey.primerlogins.kotlin.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.okey.primerlogins.kotlin.ChannelAdapter
import com.okey.primerlogins.kotlin.MeQuery
import com.okey.primerlogins.kotlin.R
import com.okey.primerlogins.kotlin.RequestTokenInterceptor
import okhttp3.OkHttpClient

//import sun.jvm.hotspot.utilities.IntArray


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    internal val TAG = "RNLOGINS"
    private val BASE_GRAPHQL_SERVER_URL = "http://10.0.2.2:4000"

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ChannelAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
            val root = inflater.inflate(R.layout.fragment_home, container, false)
            val rv_channel_list = root.findViewById<RecyclerView>(R.id.rv_channel_list)

            adapter = ChannelAdapter()
            linearLayoutManager = LinearLayoutManager(this.activity)
            rv_channel_list.layoutManager = linearLayoutManager
            rv_channel_list.adapter = adapter


       arguments?.let {
            val token = it.getString("token")

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(RequestTokenInterceptor(token!!))
                .build()
            val apolloClient = ApolloClient.builder()
                .serverUrl(BASE_GRAPHQL_SERVER_URL)
                .okHttpClient(okHttpClient)
                .build()

            val first = 3
            val after : Input<String> = Input.optional("")
            val meQuery = MeQuery(first, after)
            apolloClient.query(meQuery)
                .enqueue(object : ApolloCall.Callback<MeQuery.Data>(){

                    override fun onResponse(response: Response<MeQuery.Data>) {
                        if( response.errors().count() == 0 ) {
                            val data = response.data()
                            data?.me()?.fragments()?.channelsList_list()?.channels()?.edges()?.let {
                                val channels = it.map { edge -> edge.node()?.fragments()?.channel_item() }
                                adapter.addItems(channels)

                                activity!!.runOnUiThread {
                                    adapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }

                    override fun onFailure(e: ApolloException) {
                        Log.e(TAG, e.message)
                    }

                })

        }


        return root
    }
}